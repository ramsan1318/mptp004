
import os

#MENU
def menu():
    os.system("cls")
    print("a. Registrar productos del comercio [codigo-descripcion-precio-stock")
    print("b. Mostrar lista")
    print("c. Mostrar productos en stock [desde-hasta]")
    print("d. Ingresar un valor X para sumar a los productos en stock inferiores a un valor Y")
    print("e. Eliminar productos cuyo stock es igual a cero")
    print("f. Salir")
    x=input("Ingrese una opcion: ")
    return(x)

#FUNCION CARGAR LISTA CON DATOS DE PRODUCTO
def cargarLista():
    lista={}
    respuesta=True
    while respuesta==True:
        a=int(input("Codigo del producto: "))
        for codigo in lista: #Evita que el codigo no se repita
            if codigo==a:
                auxA=False
                while auxA==False:
                    print("El codigo ingresado ya existe")
                    a=int(input("Ingrese codigo: "))
                    if a!=codigo:
                        auxA=True

        b=input("Descripcion del producto: ")

        c=int(input("Precio: "))
        if c<0:     # Validar Ingreso de valores positivos
            auxC=False
            while auxC==False: 
                print("El precio no puede ser negativo")
                c=int(input("Ingrese precio: "))
                if c>=0:
                    auxC=True

        d=int(input("Stock: ")) #Validar ingreso de stock positivo
        if d<0:
            auxD=False
            while auxD==False:
                print("El stock no puede ser negativo")
                d=int(input("Ingrese stock: "))
                if d>=0:
                    auxD=True

        lista[a]=[b,c,d]
        aux=input("¿Desea continuar?s/n ")
        if aux=="s" or aux=="S":
            respuesta=True
        else:
            respuesta=False
    return lista

#FUNCION ESPERAR 
def espera():
    input("Presione una tecla para continuar")

#FUNCION MOSTRAR LISTA
def mostrarDatos(listaProductos):
    print("lista generada: ")
    for  clave,datos in listaProductos.items():
        print(clave,datos)

#FUNCION PRODUCTOS DESDE HASTA
def determinar(listaProductos):
    nuevaLista={}
    x=int(input("stock desde: "))
    y=int(input("stock hasta: "))
    for clave,datos in listaProductos.items():
        if (datos[2]>x and datos[2]<y):
            nuevaLista[clave]=[datos]
    return(nuevaLista)

#FUNCION SUMA DE PRODUCTOS STOCK INFERIOR A Y
def sumarStock(listaProductos,y,x):
    for clave,datos in listaProductos.items():
        if datos[2]<y:
            datos[2]+=x
    return listaProductos

#FUNCION ELIMINAR PRODUCTO STOCK IGUAL A CERO
def eliminar(listaProductos):
    lista=[]                          #Guardamos en lista las claves de valores a eliminar
    for clave,datos in listaProductos.items():
        if datos[2]==0:
            lista.append(clave) 
    for elemento in lista:             # Eliminacion de valores del diccionario por el uso de listas
        del listaProductos[elemento] 
    return(listaProductos)

#PRINCIPAL
opcion="*"
while opcion!="f":
    opcion=menu()
    if (opcion=="a" or opcion=="A"):
        print("Cargar lista de productos")
        listaProductos=cargarLista()
        print("Los datos han sido cargados correctamente")
        print(listaProductos)
        espera()
    if (opcion=="b" or opcion=="B"):
        mostrarDatos(listaProductos)
        espera()
    if (opcion=="c" or opcion=="C"):
        lista=determinar(listaProductos)
        print("Productos en stock en el intervalo [desde-hasta]: ")
        mostrarDatos(lista)
        espera()
    if (opcion=="d" or opcion=="D"):
        y=int(input("Ingrese un valor Y: "))
        x=int(input("Ingrese un valor X para sumar a los productos en stock menores a Y: "))
        lista2=sumarStock(listaProductos,y,x)
        mostrarDatos(lista2)
        espera()
    if (opcion=="e" or opcion=="E"):
        lista3=eliminar(listaProductos)
        mostrarDatos(lista3)
        espera()
    if (opcion=="f" or opcion=="F"):
        print("Fin del programa")



